<?php

namespace App\Exception;

class OperatorNotFoundException extends \Exception
{
    public $message = 'Operator not found!';
}
