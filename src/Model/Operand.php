<?php

namespace App\Model;

class Operand
{
    /** @var float */
    private $value;

    public function __construct(float $value)
    {
        $this->value = $value;
    }

    public function getValue(): float
    {
        return (float) $this->value;
    }
}
