<?php

namespace App\Controller;

use App\Service\Handler;
use App\Exception\InvalidArgumentException;
use App\Exception\OperatorNotFoundException;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CalculatorController extends AbstractController
{
    /**
     * @Route("/", name="home_page")
     */
    public function index()
    {
        return $this->render('calculator.html.twig');
    }

    /**
     * @Route("/calculate", methods={"post"})
     * @param Request $request
     * @param Handler $handler
     * @return JsonResponse
     * @throws OperatorNotFoundException
     */
    public function calculate(Request $request, Handler $handler)
    {
        try {
            $result = $handler->handle($request);
        } catch (InvalidArgumentException $e) {
            return new JsonResponse($e->getMessage(), JsonResponse::HTTP_BAD_REQUEST);
        }

        return new JsonResponse(['result' => $result, 'status' => 'success']);
    }
}
