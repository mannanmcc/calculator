<?php

namespace App\Service;

use App\Model\Operand;

class MultiplicationOperator implements OperatorInterface
{
    public function compute(Operand $operandA, Operand $operandB): float
    {
        return $operandA->getValue() * $operandB->getValue();
    }
}
