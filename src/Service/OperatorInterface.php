<?php

namespace App\Service;

use App\Model\Operand;

interface OperatorInterface
{
    public function compute(Operand $operandA, Operand $operandB): float;
}
