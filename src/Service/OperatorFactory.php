<?php

namespace App\Service;

use App\Exception\OperatorNotFoundException;

class OperatorFactory
{
    /**
     * @param string $operator
     * @return OperatorInterface
     * @throws OperatorNotFoundException
     */
    public function getOperator(string $operator): OperatorInterface
    {
        switch ($operator) {
            case 'add':
                return new AddOperator();
            case 'sub':
                return new SubtractOperator();
            case 'div':
                return new DivisionOperator();
            case 'mul':
                return new MultiplicationOperator();
            case 'and':
                return new AndOperator();
            case 'or':
                return new OrOperator();
            default:
                throw new OperatorNotFoundException();
        }
    }
}
