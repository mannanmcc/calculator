<?php

namespace App\Service;

use App\Exception\InvalidArgumentException;
use App\Exception\OperatorNotFoundException;
use App\Model\Operand;
use Symfony\Component\HttpFoundation\Request;

class Handler
{
    /** @var DataValidator */
    private $validator;

    /** @var OperatorFactory */
    private $operatorFactory;

    /** @var Calculator */
    private $calculator;

    public function __construct(DataValidator $validator, OperatorFactory $operatorFactory, Calculator $calculator)
    {
        $this->validator = $validator;
        $this->operatorFactory = $operatorFactory;
        $this->calculator = $calculator;
    }

    /**
     * @param string $postedData
     * @return array
     * @throws InvalidArgumentException
     * @throws OperatorNotFoundException
     */
    public function handle(Request $request): float
    {
        $data = $this->validator->validateAndReturnAsArray($request);
        $operandA = new Operand((float) $data['operandA']);
        $operandB = new Operand((float) $data['operandB']);
        $operator = $this->operatorFactory->getOperator($data['operator']);

        return $this->calculator->calculate($operandA, $operandB, $operator);
    }
}
