<?php

namespace App\Service;

use App\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

class DataValidator
{
    /**
     * @param Request $request
     * @return array
     * @throws InvalidArgumentException
     */
    public function validateAndReturnAsArray(Request $request) : array
    {
        if (!$operandA = $request->get('operandA')) {
            throw new InvalidArgumentException('First value must be provided');
        }

        if (!$operandB = $request->get('operandB')) {
            throw new InvalidArgumentException('Second value must be provided');
        }

        if (!$operator = $request->get('operator')) {
            throw new InvalidArgumentException('Operator must be provided');
        }

        if (!is_numeric($operandA) || !is_numeric($operandB)) {
            throw new InvalidArgumentException('Data provided must be numeric');
        }

        return [
            'operandA' => $operandA,
            'operandB' => $operandB,
            'operator' => $operator
        ];
    }
}
