<?php

namespace App\Service;

use App\Model\Operand;

class OrOperator implements OperatorInterface
{
    public function compute(Operand $operandA, Operand $operandB): float
    {
        return $operandA->getValue() | $operandB->getValue();
    }
}
