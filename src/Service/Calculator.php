<?php

namespace App\Service;

use App\Model\Operand;

class Calculator
{
    public function calculate(Operand $operandA, Operand $operandB, OperatorInterface $operator): float
    {
        return $operator->compute($operandA, $operandB);
    }
}
