$(".button").click(function(e) {
    var calculateEndPointUrl = "/calculate";

    operandA = $('#operandA').val();
    if (!operandA) {
        alert("Please put the first value");
        return;
    }

    operandB = $('#operandB').val();
    if (!operandB) {
        alert("please put the second value");
        return;
    }

    operator = $(this).attr('id');
    var dataString = "operandA=" + operandA + "&operandB=" + operandB + "&operator=" + operator;

    $.ajax({
        type: "POST",
        url: calculateEndPointUrl,
        data: dataString,
        success: function(data)
        {
            $("#result").html(data.result);
        },
        error: function (xhr) {
            $("#result").html(xhr.responseText);
        }
    });

    e.preventDefault();
});